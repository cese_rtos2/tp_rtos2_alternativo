/*
 * taskRecording.h
 *
 *  Created on: 27 jun. 2022
 *      Author: javelasco
 */

#ifndef INC_TASKSTARTRECORDING_H_
#define INC_TASKSTARTRECORDING_H_

#ifdef __cplusplus
 extern "C" {
#endif

 // ------ inclusions ---------------------------------------------------

 // ------ macros -------------------------------------------------------

 // ------ typedef ------------------------------------------------------

 // ------ external data declaration ------------------------------------

 // ------ external functions declaration -------------------------------

 void vTaskStartRecording (void *pvParameters);


 #ifdef __cplusplus
 }
#endif

#endif /* INC_TASKSTARTRECORDING_H_ */
