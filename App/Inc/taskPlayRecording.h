/*
 * taskPlayback.h
 *
 *  Created on: 27 jun. 2022
 *      Author: javelasco
 */

#ifndef INC_TASKPLAYRECORDING_H_
#define INC_TASKPLAYRECORDING_H_

#ifdef __cplusplus
 extern "C" {
#endif

 // ------ inclusions ---------------------------------------------------

 // ------ macros -------------------------------------------------------

 // ------ typedef ------------------------------------------------------

 // ------ external data declaration ------------------------------------

 // ------ external functions declaration -------------------------------

 void vTaskPlayRecording (void *pvParameters);


 #ifdef __cplusplus
 }
#endif

#endif /* INC_TASKPLAYRECORDING_H_ */
