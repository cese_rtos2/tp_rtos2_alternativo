/*
 * list.h
 *
 *  Created on: 20 jul. 2022
 *      Author: javelasco
 */

#ifndef INC_LIST_H_
#define INC_LIST_H_

/* Project includes. */
#include "main.h"
#include "cmsis_os.h"

/* Standard includes. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/* Demo includes. */
#include "appConfigs.h"
#include "supporting_Functions.h"


 typedef enum {
	PLAYING_RECORDING_SUCCESSFUL = 0,
	PLAYING_STOPPED_SUCCESSFUL = 5,
	OK_DELETE_MSG = 0,
	ERROR_LIST_EMPTY = 1,
	ERROR_DELETE_MSG = 1,
	ERROR_NUM_OUT_RANGE = 2,
	ERROR_NUM_INCORRECT = 3,
	ERROR_UNKNOWN = 4
} State_t;

typedef struct Data_t {
	uint8_t		index;
	bool 		value;
} Data_t;

typedef struct Node_t {
	struct Data_t	*data;
	uint8_t 		dataLength;
	uint8_t 		totalIndexOfNode;
	struct Node_t	*sig;
} Node_t;

typedef struct Msg_t {
	uint8_t 		msgIdent;
	struct Node_t*	node;
	uint16_t		totalIndexOfMsg;
	struct Msg_t*	sig;
} Msg_t;


Node_t* List_AddNode(Node_t** pNode);

Msg_t* List_AddMsg(Msg_t** pMsg);

Msg_t* List_SaveMsg( Msg_t** pMsg_temp, Msg_t** pMsg );

void List_FreeMemory(Node_t** p);

uint8_t List_CountMsgs(Msg_t* pMsg);

State_t List_PlayMsg(Msg_t* pMsg, const uint8_t numMsg);

State_t List_DeleteMsg(Msg_t** pMsg, const uint8_t numMsg);



#endif /* INC_LIST_H_ */
