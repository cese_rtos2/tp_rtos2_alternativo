/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file   : uart_interrupt_driver.h
 * @date   : May 23, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

#ifndef INC_UART_INTERRUPT_DRIVER_H_
#define INC_UART_INTERRUPT_DRIVER_H_

/********************** CPP guard ********************************************/
#ifdef __cplusplus
extern "C" {
#endif

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "FreeRTOS.h"
#include "semphr.h"

/********************** macros ***********************************************/

/********************** typedef **********************************************/

typedef struct
{
    UART_HandleTypeDef *uart;

    struct
    {
      uint8_t *buffer;
      uint32_t buffer_size;
    } rx;

    struct
    {
      uint8_t *buffer;
      uint32_t buffer_size;
    } tx;

} uart_interrupt_driver_config_t;

typedef struct
{
    UART_HandleTypeDef *uart;

    struct
    {
      uint8_t *buffer;
      uint32_t buffer_size;
      uint16_t size;
      SemaphoreHandle_t semaphore;
    } rx;

    struct
    {
      uint8_t *buffer;
      uint32_t buffer_size;
      SemaphoreHandle_t mutex;
    } tx;

} uart_interrupt_driver_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void uart_interrupt_driver_tx_isr(uart_interrupt_driver_t *self, UART_HandleTypeDef *huart);

void uart_interrupt_driver_rx_isr(uart_interrupt_driver_t *self, UART_HandleTypeDef *huart, uint16_t size);

//void uart_interrupt_driver_systick_isr(void);

//uint16_t uart_interrupt_driver_send(uart_interrupt_driver_t *self, uint8_t const *const buffer, uint16_t size);
uint16_t uart_interrupt_driver_send(uart_interrupt_driver_t *self, uint8_t const *const buffer, uint16_t size, TickType_t xTicksToWait);

uint16_t uart_interrupt_driver_receive(uart_interrupt_driver_t *self, uint8_t const *const buffer, TickType_t xTicksToWait);

void uart_interrupt_driver_init(uart_interrupt_driver_t *self, uart_interrupt_driver_config_t *config);

/********************** End of CPP guard *************************************/
#ifdef __cplusplus
}
#endif

#endif /* INC_UART_INTERRUPT_DRIVER_H_ */
/********************** end of file ******************************************/

