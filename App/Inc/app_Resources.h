/*--------------------------------------------------------------------*-

    app_Resources.h (Released 2022-06)

  --------------------------------------------------------------------

    This is the Tasks Header file.

-*--------------------------------------------------------------------*/



#ifndef __APP_RESOURCES_H
#define __APP_RESOURCES_H

#include <myList.h>

#ifdef __cplusplus
 extern "C" {
#endif

// ------ inclusions ---------------------------------------------------

// ------ macros -------------------------------------------------------

// ------ typedef ------------------------------------------------------
 typedef uint32_t 	tick_t;

// ------ external data declaration ------------------------------------
 extern Msg_t* p;
 extern Msg_t* p_temp;
 extern bool flagStop, flagCommandSerial;

/* Used to hold the handle of TaskTest. */
 extern xTaskHandle vTaskStartHandle;
 extern xTaskHandle vTaskUARTtHandle;
 extern xTaskHandle vTaskLedHandle;
 extern xTaskHandle vTaskStartRecordingHandle;
 extern xTaskHandle vTaskPlayRecordingHandle;

 extern xSemaphoreHandle xBinarySemaphoreStart;
 extern xSemaphoreHandle xBinarySemaphoreStartRecording;
 extern xSemaphoreHandle xBinarySemaphorePlayRecording;
 //extern xSemaphoreHandle xBinarySemaphoreLed;
 extern xSemaphoreHandle xMutex;
 extern xSemaphoreHandle xMutexSerial;
 extern xQueueHandle xQueuePlayRecording;

// ------ external functions declaration -------------------------------

#ifdef __cplusplus
}
#endif

#endif /* __APP_RESOURCES_H */

/*------------------------------------------------------------------*-
  ---- END OF FILE -------------------------------------------------
-*------------------------------------------------------------------*/
