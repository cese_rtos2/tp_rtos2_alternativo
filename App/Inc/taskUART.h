/*
 * taskUART.h
 *
 *  Created on: 7 ago. 2022
 *      Author: jhonn
 */

#ifndef INC_TASKUART_H_
#define INC_TASKUART_H_

/********************** CPP guard ********************************************/
#ifdef __cplusplus
extern "C" {
#endif

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void vTaskUART(void *pvParameters);

/********************** End of CPP guard *************************************/
#ifdef __cplusplus
}
#endif

#endif /* INC_TASKUART_H_ */
