/*
 * taskLed.h
 *
 *  Created on: Jun 27, 2022
 *      Author: javelasco
 */

#ifndef INC_TASKLED_H_
#define INC_TASKLED_H_

#ifdef __cplusplus
 extern "C" {
#endif

 // ------ inclusions ---------------------------------------------------

 // ------ macros -------------------------------------------------------

 // ------ typedef ------------------------------------------------------

 // ------ external data declaration ------------------------------------

 // ------ external functions declaration -------------------------------

 void vTaskLed (void *pvParameters);


 #ifdef __cplusplus
 }
#endif

#endif /* INC_TASKLED_H_ */
