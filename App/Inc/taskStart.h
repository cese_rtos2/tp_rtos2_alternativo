/*
 * taskStart.h
 *
 *  Created on: 27 jun. 2022
 *      Author: javelasco
 */

#include <myList.h>

#ifndef INC_TASKSTART_H_
#define INC_TASKSTART_H_

#ifdef __cplusplus
 extern "C" {
#endif

 // ------ inclusions ---------------------------------------------------

 // ------ macros -------------------------------------------------------

 // ------ typedef ------------------------------------------------------

 // ------ external data declaration ------------------------------------

 // ------ external functions declaration -------------------------------

 void vTaskStart (void* pvParameters);


 #ifdef __cplusplus
 }
#endif

#endif /* INC_TASKSTART_H_ */
