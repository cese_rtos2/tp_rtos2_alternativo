/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : routine_example_command.c
 * @date   : May 18, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <arch.h>
#include <command.h>
#include <appConfigs.h>
#include <app_Resources.h>
#include <myList.h>

/********************** macros and definitions *******************************/

#define PERIOD_ticks_                   (20 / APP_CONFIG_PERIOD_ms)
#define DELAY_ticks_                    (500 / APP_CONFIG_PERIOD_ms)

#define BUFFER_SIZE_                    (64)
/*
 * 115200 / (10 * 1000) = 11.52 bytes/ms = aprox 16 = 2^4
 * 64 / 16 = 4
 */
#define TIMEOUT_RX_MS_                 (100)
#define TIMEOUT_TX_MS_(len)            ((len) * 20)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/


/********************** external data definition *****************************/

extern UART_HandleTypeDef huart3;

/********************** internal functions definition ************************/

static void uart_string_write_(const char *str) {
  size_t len = strlen(str);

  HAL_StatusTypeDef tx_status;
  tx_status = HAL_UART_Transmit(&huart3, (uint8_t*)str, len, TIMEOUT_TX_MS_(len));
  //tx_status = HAL_UART_Transmit_IT(&huart3, (uint8_t*)str, len);

  if (HAL_OK != tx_status) {
    // error
  }
}

/********************** external functions definition ************************/

void command_start_handler(unsigned int delay) {
	xSemaphoreTake(xMutexSerial, portMAX_DELAY);
	if(flagCommandSerial) {
		xSemaphoreGive(xMutexSerial);
		vTaskSuspend(vTaskStartHandle);
		uart_string_write_("command_start_handler\n\r");

		char str[32];
		sprintf(str, " delay: %d\n\r", delay);
		uart_string_write_(str);

		vPrintStringAndNumber( " Number of stored messages: ", List_CountMsgs(p));
		vPrintString( "\n\r" );

		if(List_AddMsg(&p_temp) != NULL) {
			uart_string_write_( "New message and node created!\n\r" );
			osDelay(delay);
			//uart_string_write_( "Give semaphore StartRecording\n\r" );
			uart_string_write_( "Recording...\n\r\n\r" );

			vTaskResume(vTaskLedHandle);
			xSemaphoreTake(xMutexSerial, portMAX_DELAY);
				flagCommandSerial = false;
			xSemaphoreGive(xMutexSerial);
			xSemaphoreGive( xBinarySemaphoreStartRecording );
		} else {
			vTaskResume(vTaskStartHandle);
			uart_string_write_( "The menssage and node was not created. Dont memory available!\n\r" );
			vPrintStringAndNumber( "Number of stored messages: ", List_CountMsgs(p_temp));
			vPrintString( "\n\r" );
		}
	}
	xSemaphoreGive(xMutexSerial);
}

void command_stop_handler(unsigned int delay) {
	uart_string_write_("command_stop_handler\n\r");

	char str[32];
	sprintf(str, " delay: %d\n\r", delay);
	uart_string_write_(str);

	osDelay(delay);

	xSemaphoreTake(xMutex, portMAX_DELAY);
		flagStop = true;
	xSemaphoreGive(xMutex);
}

void command_save_handler(void) {
	xSemaphoreTake(xMutexSerial, portMAX_DELAY);
	if(flagCommandSerial) {
		xSemaphoreGive(xMutexSerial);
		uart_string_write_("command_save_handler\n\r");

		if(List_SaveMsg(&p_temp, &p) != NULL)
			uart_string_write_( "Message saved successfully.\n\r" );
		else
			uart_string_write_( "There are no messages to save.\n\r" );
	}
	xSemaphoreGive(xMutexSerial);
}

void command_play_handler(unsigned int id) {
	xSemaphoreTake(xMutexSerial, portMAX_DELAY);
	if(flagCommandSerial) {
		xSemaphoreGive(xMutexSerial);
		uart_string_write_("command_play_handler\n\r");

		char str[32];
		sprintf(str, " id: %d\n\r", id);
		uart_string_write_(str);

		//xSemaphoreGive( xBinarySemaphorePlayRecording );
		//BaseType_t xStatus = xQueueSend(xQueuePlayRecording, (void *)&p, 0);
		//if(xStatus != pdPASS)
		//		vPrintString( "Could not send to the queue.\r\n" );

		switch( List_PlayMsg( p, id ) ) {
			case ERROR_LIST_EMPTY:
				uart_string_write_( " Error! List empty.\n\r" );
				break;
			case ERROR_NUM_OUT_RANGE:
				uart_string_write_( " Error! Parameter incorrect.\n\r" );
				break;
			case ERROR_NUM_INCORRECT:
				uart_string_write_( " Error! Message ID is incorrect.\n\r" );
				break;
			case PLAYING_RECORDING_SUCCESSFUL:
				uart_string_write_( "Ok! Played recording successfully.\n\r" );
				break;
			case PLAYING_STOPPED_SUCCESSFUL:
				uart_string_write_( "Ok! Playing stopped successfully.\n\r" );
				break;
			default:
				break;
		}; //End Switch
	}
	xSemaphoreGive(xMutexSerial);
}

void command_delete_handler(unsigned int id) {
	xSemaphoreTake(xMutexSerial, portMAX_DELAY);
	if(flagCommandSerial) {
		xSemaphoreGive(xMutexSerial);
		uart_string_write_("command_delete_handler\n\r");

		char str[32];
		sprintf(str, " id: %d\n\r", id);
		uart_string_write_(str);

		switch( List_DeleteMsg(&p, id) ) {
			case OK_DELETE_MSG:
				uart_string_write_( " Message deleted successfully.\n\r" );
				break;
			case ERROR_DELETE_MSG:
				uart_string_write_( " Error! Message ID is incorrect.\n\r" );
				break;
			default:
				break;
		}
	}
	xSemaphoreGive(xMutexSerial);
}

void command_list_handler(void) {
	xSemaphoreTake(xMutexSerial, portMAX_DELAY);
	if(flagCommandSerial) {
		xSemaphoreGive(xMutexSerial);
		uart_string_write_("command_list_handler\n\r");
		uint8_t numList = List_CountMsgs(p);

		if(numList > 0) {
			char buff[50];
			Msg_t *pMsg_aux = p;
			while( pMsg_aux != NULL ) {
				sprintf(buff, " - Message %d: %d bytes.\n\r", pMsg_aux->msgIdent, pMsg_aux->totalIndexOfMsg);
				uart_string_write_(buff);
				pMsg_aux = pMsg_aux->sig;
			}
		} else {
			uart_string_write_(" There aren't messages!\n\r");
		}
	}
	xSemaphoreGive(xMutexSerial);
}

void command_size_handler(void) {
	xSemaphoreTake(xMutexSerial, portMAX_DELAY);
	if(flagCommandSerial) {
		xSemaphoreGive(xMutexSerial);
		uart_string_write_("command_size_handler\n\r");
		uint8_t numList = List_CountMsgs(p);

		char buff[50];
		sprintf(buff, " There are %d messages stored in the list.\n\r", numList);
		uart_string_write_(buff);
	}
	xSemaphoreGive(xMutexSerial);
}

void command_char_out(char ch) {
  HAL_StatusTypeDef tx_status;
  tx_status = HAL_UART_Transmit(&huart3, (uint8_t*)&ch, 1, TIMEOUT_TX_MS_(1));

  if (HAL_OK != tx_status) {
    // error
    uart_string_write_("Error");
  }
}

void init_interface_uart(void *parameters) {
	command_init();
}

/********************** end of file ******************************************/
