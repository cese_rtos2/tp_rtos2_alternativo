/*
 * taskLed.c
 *
 *  Created on: Jun 27, 2022
 *      Author: javelasco
 */

/* Project includes. */
#include "main.h"
#include "cmsis_os.h"

/* Standard includes. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/* Demo includes. */
#include "supporting_Functions.h"

/* Application includes. */
#include "app_Resources.h"
#include "appConfigs.h"
#include "taskLed.h"

void vTaskLed ( void *pvParameters )
{

	vPrintString( "Begin TaskLed\n\r" );
	vTaskSuspend(vTaskLedHandle);

	while( 1 )
	{
			HAL_GPIO_TogglePin(GPIOB, LD2_Pin);
			osDelay(BLINKING_DELAY_MS);
	}
}
