/*
 * taskPlayback.c
 *
 *  Created on: 27 jun. 2022
 *      Author: javelasco
 */

/* Project includes. */
#include "main.h"
#include "cmsis_os.h"

/* Standard includes. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/* Demo includes. */
#include "supporting_Functions.h"

/* Application includes. */
#include "app_Resources.h"
#include "appConfigs.h"
#include "taskPlayRecording.h"

/* Functions includes. */
static void stopPlayRecoding();

static tick_t 	myTicks;
static Msg_t*	pMsg_aux;
static Node_t* 	pNode_aux;
static Data_t*	pData_aux;
uint16_t i=0;

void vTaskPlayRecording( void *pvParameters ) {
	vPrintString( "Begin TaskPlayRecording\n\r" );
	xSemaphoreTake( xBinarySemaphorePlayRecording, portMAX_DELAY );

	while( 1 ) {
		xSemaphoreTake( xBinarySemaphorePlayRecording, 0 );

		/*BaseType_t xStatus = xQueueReceive(xQueuePlayRecording, &pMsg_aux, portMAX_DELAY);
		if(xStatus != pdPASS)
			vPrintString( "Could not receive from the queue.\r\n" );*/
		pMsg_aux = p_temp;
		while( pMsg_aux->sig != NULL )	pMsg_aux = pMsg_aux->sig;


		pNode_aux = pMsg_aux->node;
		pData_aux = pNode_aux->data;

		HAL_GPIO_WritePin( GPIOB, LD2_Pin, GPIO_PIN_SET);

		while(pNode_aux != NULL) {
			i=0;
			while(i < pNode_aux->totalIndexOfNode) {
				myTicks = HAL_GetTick();
				while ( HAL_GetTick() < myTicks + RECORDING_SAMPLE_RATE_MS )
					osDelay( 1 );

				vPrintStringAndNumber( "Read:", ( pData_aux + i )->value );

				if(( ( pData_aux + i )->value ) == 1 ) {
					HAL_GPIO_WritePin( GPIOB, LD3_Pin, GPIO_PIN_SET );
				} else {
					HAL_GPIO_WritePin( GPIOB, LD3_Pin, GPIO_PIN_RESET );
				}

				i++;
			} //End While
			pNode_aux = pNode_aux->sig;
			pData_aux = pNode_aux->data;
		} //End While
		stopPlayRecoding();
	} //End While
} //End TaskPlayback

static void stopPlayRecoding() {
	HAL_GPIO_WritePin( GPIOB, LD2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin( GPIOB, LD3_Pin, GPIO_PIN_RESET);
	pNode_aux = NULL;

	osDelay(1000);

	xSemaphoreTake(xMutexSerial, portMAX_DELAY);
		flagCommandSerial = true;
	xSemaphoreGive(xMutexSerial);

	vPrintString( "Do you want to save the message? Send save command.\n\r" );
	vTaskResume(vTaskStartHandle);
	xSemaphoreGive( xBinarySemaphoreStart );
	xSemaphoreTake( xBinarySemaphorePlayRecording, portMAX_DELAY );
}


