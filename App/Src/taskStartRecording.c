/*
 * taskRecording.c
 *
 *  Created on: 27 jun. 2022
 *      Author: javelasco
 */

/* Project includes. */
#include "main.h"
#include "cmsis_os.h"

/* Standard includes. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/* Demo includes. */
#include "supporting_Functions.h"

/* Application includes. */
#include "app_Resources.h"
#include "appConfigs.h"
#include <taskStartRecording.h>

/* Functions includes. */
static void stopRecording();

static tick_t myTicks2;
static tick_t myTicks;

static Msg_t *pMsg_aux;
static Node_t *pNode_aux;
static Data_t *pData_aux;

static uint16_t count;
static uint8_t i;

void vTaskStartRecording( void *pvParameters ) {
	vPrintString( "Begin TaskStartRecording\n\r" );
	xSemaphoreTake( xBinarySemaphoreStartRecording, portMAX_DELAY );

	while( 1 ) {
		xSemaphoreTake( xBinarySemaphoreStartRecording, 0 );

		pMsg_aux = p_temp;

		while( pMsg_aux->sig != NULL )	pMsg_aux = pMsg_aux->sig;

		pNode_aux = pMsg_aux->node;
		pData_aux = pNode_aux->data;

		xSemaphoreTake(xMutex, portMAX_DELAY);
			flagStop = false;
		xSemaphoreGive(xMutex);

		count = 0;
		myTicks2 = HAL_GetTick();

		while( pNode_aux != NULL ) {
			i=0;

			xSemaphoreTake(xMutex, portMAX_DELAY);
			while( (i < pNode_aux->dataLength) && !flagStop ) {
				xSemaphoreGive(xMutex);
				myTicks = HAL_GetTick();
				while ( HAL_GetTick() < myTicks + RECORDING_SAMPLE_RATE_MS )
					osDelay( 1 );

				if( HAL_GPIO_ReadPin( USER_Btn_GPIO_Port, USER_Btn_Pin ) ) {
					HAL_GPIO_WritePin( GPIOB, LD3_Pin, GPIO_PIN_SET );
					( pData_aux + i )->value = (bool)GPIO_PIN_SET;

					vPrintStringAndNumber( "Write: ", ( pData_aux + i )->value );

					pNode_aux->totalIndexOfNode++;
					( pData_aux + i )->index = i;
					i++;
					count++;
					myTicks2 = HAL_GetTick();
				} else {
					HAL_GPIO_WritePin( GPIOB, LD3_Pin, GPIO_PIN_RESET );
					( pData_aux + i )->value = (bool)GPIO_PIN_RESET;

					vPrintStringAndNumber( "Write: ", ( pData_aux + i )->value  );

					pNode_aux->totalIndexOfNode++;
					( pData_aux + i )->index = i;
					i++;
					count++;

					if( HAL_GetTick() >= myTicks2 + DOWNTIME_MS ) {
						xSemaphoreTake(xMutex, portMAX_DELAY);
							flagStop = true;
						xSemaphoreGive(xMutex);
						break;
					} //End If
				} //End Else
			} //End While
			xSemaphoreGive(xMutex);
			xSemaphoreTake(xMutex, portMAX_DELAY);
			if( !flagStop ) {
				xSemaphoreGive(xMutex);
				if( List_AddNode(&pNode_aux) != NULL ) {
					//vPrintString( "New node created!\n\r" );
					if( pNode_aux->sig != NULL )	pNode_aux = pNode_aux->sig;
					pData_aux = pNode_aux->data;
				} else {
					vPrintString( "Dont memory available!\n\r" );
					pMsg_aux->totalIndexOfMsg = count - 1;
					vPrintStringAndNumber( "Was written: ", pMsg_aux->totalIndexOfMsg );
					stopRecording();
					break;
				}
			} else {
				xSemaphoreGive(xMutex);
				pMsg_aux->totalIndexOfMsg = count - 1;
				vPrintStringAndNumber( "Was written: ", pMsg_aux->totalIndexOfMsg );
				stopRecording();
				break;
			}
		} //End While
	} //End While
} //End TaskRecording


static void stopRecording() {
	vTaskSuspend(vTaskLedHandle);
	HAL_GPIO_WritePin( GPIOB, LD2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin( GPIOB, LD3_Pin, GPIO_PIN_RESET);
	pNode_aux = NULL;

	xSemaphoreTake(xMutex, portMAX_DELAY);
		flagStop = false;
	xSemaphoreGive(xMutex);

	//vPrintString( "Give semaphore PlayRecording\n\r" );
	vPrintString( "Playing recording!\n\r" );
	osDelay(1000);

	xSemaphoreGive( xBinarySemaphorePlayRecording );
	/*BaseType_t xStatus = xQueueSend(xQueuePlayRecording, (void *)&p_temp, 0);
	if(xStatus != pdPASS)
		vPrintString( "Could not send to the queue.\r\n" );*/

	xSemaphoreTake( xBinarySemaphoreStartRecording, portMAX_DELAY );
}
