/*
 * list.c
 *
 *  Created on: 20 jul. 2022
 *      Author: javelasco
 */
#include <myList.h>
#include "app_Resources.h"

Node_t* List_AddNode(Node_t** pNode) {
	Data_t* newData = ( Data_t* )pvPortMalloc( NODE_LENGHT * sizeof( Data_t ) );
	if(newData == NULL)	return NULL;
	newData->index = 0;
	newData->value = 0;

	Node_t* newNode = ( Node_t* )pvPortMalloc( sizeof( Node_t ) );
	if(newNode == NULL)	return NULL;
	newNode->data = newData;
	newNode->dataLength = NODE_LENGHT;
	newNode->totalIndexOfNode = 0;
	newNode->sig = NULL;

	if( *pNode == NULL ) {
		*pNode = newNode;
	} else {
		Node_t* pNode_aux = *pNode;
		while( pNode_aux->sig != NULL )	pNode_aux = pNode_aux->sig;
		pNode_aux->sig = newNode;
	}
	return newNode;
}

Msg_t* List_AddMsg(Msg_t** pMsg) {
	if(*pMsg == NULL) {
		Data_t* newData = ( Data_t* )pvPortMalloc( NODE_LENGHT * sizeof( Data_t ) );
		if(newData == NULL)	return NULL;
		newData->index = 0;
		newData->value = 0;

		Node_t* newNode = ( Node_t* )pvPortMalloc( sizeof( Node_t ) );
		if(newNode == NULL)	return NULL;
		newNode->data = newData;
		newNode->dataLength = NODE_LENGHT;
		newNode->totalIndexOfNode = 0;
		newNode->sig = NULL;

		Msg_t* newMsg = ( Msg_t* )pvPortMalloc( sizeof( Msg_t ) );
		if(newMsg == NULL)	return NULL;
		newMsg->node = newNode;
		newMsg->totalIndexOfMsg = 0;
		newMsg->sig = NULL;

		newMsg->msgIdent = 1;
		*pMsg = newMsg;

		return newMsg;
	} else {
		switch( List_DeleteMsg(pMsg, 1) ) {
			case OK_DELETE_MSG:
				vPrintString( "Temporary message deleted successfully.\n\r" );
		  		break;
		  	case ERROR_DELETE_MSG:
		  		vPrintString( "Error! Dont temporary message deleted.\n\r" );
		  		break;
		  	default:
		  		break;
		}
		return List_AddMsg(pMsg);
	}
}

Msg_t* List_SaveMsg( Msg_t** pMsg_temp, Msg_t** pMsg ) {
	if(*pMsg == NULL) {
		*pMsg = *pMsg_temp;
		*pMsg_temp = NULL;
		return *pMsg;
	} else {
		Msg_t* pMsg_aux = *pMsg;
		while( pMsg_aux->sig != NULL )	pMsg_aux = pMsg_aux->sig;

		pMsg_aux->sig = *pMsg_temp;
		*pMsg_temp = NULL;
		uint8_t i = pMsg_aux->msgIdent + 1;

		pMsg_aux = pMsg_aux->sig;
		pMsg_aux->msgIdent = i;
		return pMsg_aux;
	}
}

uint8_t List_CountMsgs( Msg_t* pMsg) {
	uint8_t count = 0;
	Msg_t *pMsg_aux = pMsg;

	while( pMsg_aux != NULL ) {
		count++;
		pMsg_aux = pMsg_aux->sig;
	}
	return count;
}

State_t List_PlayMsg( Msg_t* pMsg, const uint8_t numMsg ) {
	if( List_CountMsgs( pMsg ) == 0 )
		return ERROR_LIST_EMPTY;

	if( numMsg == 0 )
		return ERROR_NUM_OUT_RANGE;

	uint32_t myTicks;
	Msg_t* pMsg_aux = pMsg;

	while( pMsg_aux->msgIdent != numMsg ) {
		if( pMsg_aux == NULL )
			return ERROR_NUM_INCORRECT;
		pMsg_aux = pMsg_aux->sig;
	}

	vPrintString( "\n\rPlaying recording...\n\r\n\r" );

	Node_t* pNode_aux = pMsg_aux->node;
	while( pNode_aux != NULL ) {
		HAL_GPIO_WritePin( GPIOB, LD2_Pin, GPIO_PIN_SET );

		uint16_t i=0;
		while( i < ( pNode_aux->totalIndexOfNode) ) {

			myTicks = HAL_GetTick();
			while ( HAL_GetTick() < myTicks + RECORDING_SAMPLE_RATE_MS )
				osDelay( 1 );

			vPrintStringAndNumber( "Read:", ( pNode_aux->data + i )->value );

			if( ( ( pNode_aux->data + i )->value ) == 1 )
				HAL_GPIO_WritePin( GPIOB, LD3_Pin, GPIO_PIN_SET );
			else
				HAL_GPIO_WritePin( GPIOB, LD3_Pin, GPIO_PIN_RESET );
			i++;
		} //End While

		pNode_aux = pNode_aux->sig;
	}

	HAL_GPIO_WritePin( GPIOB, LD2_Pin, GPIO_PIN_RESET );
	HAL_GPIO_WritePin( GPIOB, LD3_Pin, GPIO_PIN_RESET );
	return PLAYING_RECORDING_SUCCESSFUL;
}

State_t List_DeleteMsg(Msg_t** pMsg, const uint8_t numMsg) {

	if(numMsg > 0) {
		Msg_t* pMsg_aux = *pMsg;
		Msg_t* pMsg_aux_prev = NULL;

		while( (pMsg_aux != NULL) && (pMsg_aux->msgIdent != numMsg) ) {
			pMsg_aux_prev = pMsg_aux;
			pMsg_aux = pMsg_aux->sig;
		}

		if(pMsg_aux != NULL) {
			if(pMsg_aux_prev != NULL) {
				pMsg_aux_prev->sig = pMsg_aux->sig;
			} else {
				*pMsg = pMsg_aux->sig;
			}
		} else {
			return ERROR_DELETE_MSG;
		}

		Node_t* pNode_aux = pMsg_aux->node;
		Node_t* pNode_aux_sig = NULL;

		while(pNode_aux != NULL) {
			pNode_aux_sig = pNode_aux->sig;
			vPortFree(pNode_aux->data);
			vPortFree(pNode_aux);
			pNode_aux = pNode_aux_sig;
		}
		vPortFree(pMsg_aux);
		return OK_DELETE_MSG;
	} else {
		return ERROR_DELETE_MSG;
	}
}
