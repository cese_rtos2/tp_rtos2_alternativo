/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : uart_interrupt_driver.c
 * @date   : May 23, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <arch.h>

#include "FreeRTOS.h"
#include "semphr.h"

#include <uart_interrupt_driver.h>

#define TIMEOUT_TX_MS_(len)            ((len) * 20)

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

//static void device_init_(void)
//{
//  static bool already_initialized = false;
//  if(true == already_initialized)
//  {
//    return;
//  }
//  already_initialized = true;
//
//  for(int i = 0; i < UART_INTERRUPT_DRIVER_CONFIG_DEVICES; ++i)
//  {
//    device_.drivers[i] = NULL;
//  }
//  device_.drivers_size = 0;
//}

//static void device_driver_init_(uart_interrupt_driver_t *driver)
//{
//  while(UART_INTERRUPT_DRIVER_CONFIG_DEVICES <= device_.drivers_size);
//
//  device_.drivers[device_.drivers_size] = driver;
//  device_.drivers_size++;
//}

//static void driver_tx_isr_(uart_interrupt_driver_t *self, UART_HandleTypeDef *huart)
//{
//  if (huart->Instance != self->uart->Instance)
//  {
//    return;
//  }
//
//  static BaseType_t xHigherPriorityTaskWoken;
//  xSemaphoreGiveFromISR(self->tx.mutex, &xHigherPriorityTaskWoken);
//}

//static void driver_rx_isr_(uart_interrupt_driver_t *self, UART_HandleTypeDef *huart, uint16_t size)
//{
//  if (huart->Instance != self->uart->Instance)
//  {
//    return;
//  }
//
//  self->rx.user_cb(self->rx.buffer, size);
//
//  uart_interrupt_driver_receive_start(self);
//}

static void receive_start_(uart_interrupt_driver_t *self) {
  HAL_StatusTypeDef rx_status;
  rx_status = HAL_UARTEx_ReceiveToIdle_IT(self->uart, self->rx.buffer, self->rx.buffer_size);
  if(HAL_OK != rx_status) {
    // error
  }
}

/********************** external functions definition ************************/

void uart_interrupt_driver_tx_isr(uart_interrupt_driver_t *self, UART_HandleTypeDef *huart)
{
  if (huart->Instance != self->uart->Instance)
  {
    return;
  }

  static BaseType_t xHigherPriorityTaskWoken;
  xSemaphoreGiveFromISR(self->tx.mutex, &xHigherPriorityTaskWoken);
}

void uart_interrupt_driver_rx_isr(uart_interrupt_driver_t *self, UART_HandleTypeDef *huart, uint16_t size)
{
  if (huart->Instance != self->uart->Instance)
  {
    return;
  }

  self->rx.size = size;

  static BaseType_t xHigherPriorityTaskWoken;
  xSemaphoreGiveFromISR(self->rx.semaphore, &xHigherPriorityTaskWoken);
}

uint16_t uart_interrupt_driver_send(uart_interrupt_driver_t *self, uint8_t const *const buffer, uint16_t size, TickType_t xTicksToWait)
{
  uint16_t size_to_copy = 0;
  if(0 == size)
  {
    return 0;
  }

  if(pdTRUE == xSemaphoreTake(self->tx.mutex, xTicksToWait))
  {
    size_to_copy = (size <= self->tx.buffer_size) ? size : self->tx.buffer_size;
    memcpy(self->tx.buffer, buffer, size_to_copy);

    HAL_StatusTypeDef tx_status;
    //tx_status = HAL_UART_Transmit_IT(self->uart, self->tx.buffer, size_to_copy);
    tx_status = HAL_UART_Transmit(self->uart, self->tx.buffer, self->tx.buffer_size, TIMEOUT_TX_MS_(self->tx.buffer_size));
    if(HAL_OK != tx_status)
    {
      // error
    }

    return size_to_copy;
  }

  return 0;
}

uint16_t uart_interrupt_driver_receive(uart_interrupt_driver_t *self, uint8_t const *const buffer, TickType_t xTicksToWait)
{
  if(pdTRUE == xSemaphoreTake(self->rx.semaphore, xTicksToWait))
  {
    memcpy(buffer, self->rx.buffer, self->rx.size);
    receive_start_(self);
    return self->rx.size;
  }
  return 0;
}

void uart_interrupt_driver_init(uart_interrupt_driver_t *self, uart_interrupt_driver_config_t *config)
{
//  device_init_();

  self->uart = config->uart;

  self->rx.buffer = config->rx.buffer;
  self->rx.buffer_size = config->rx.buffer_size;

  vSemaphoreCreateBinary(self->rx.semaphore);
  if(NULL == self->rx.semaphore)
  {
      // problem !!!
  }
  xSemaphoreTake(self->rx.semaphore, 0);
  self->rx.size = 0;

  self->tx.buffer = config->tx.buffer;
  self->tx.buffer_size = config->tx.buffer_size;

  vSemaphoreCreateBinary(self->tx.mutex);
  if(NULL == self->tx.mutex)
  {
      // problem !!!
  }
  xSemaphoreGive(self->tx.mutex);

//  device_driver_init_(self);

  receive_start_(self);
}

/********************** end of file ******************************************/
