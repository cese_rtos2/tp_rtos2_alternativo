/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : task_eco.c
 * @date   : May 17, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/* Project includes. */
#include "main.h"
#include "cmsis_os.h"

/* Standard includes. */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

/* Demo includes. */
#include "supporting_Functions.h"
#include <uart_interrupt_driver.h>
#include <routine_example_command.h>
#include "command.h"

/********************** macros and definitions *******************************/

#define BUFFER_SIZE_                    (64)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static uint8_t rx_buffer_[BUFFER_SIZE_];
static uint8_t tx_buffer_[BUFFER_SIZE_];
static uint8_t user_buffer_[BUFFER_SIZE_];
static uint8_t user_buffer_decode_[BUFFER_SIZE_];
static uart_interrupt_driver_t driver_;

/********************** external data definition *****************************/

extern UART_HandleTypeDef huart3;

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
  uart_interrupt_driver_tx_isr(&driver_, huart);
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size) {
  uart_interrupt_driver_rx_isr(&driver_, huart, Size);
}

bool decode_message(uint16_t size, char caracter) {
	if( (user_buffer_[0] == caracter) && (user_buffer_[size-2] == caracter) ) {
		uint16_t i;
		for(i=0; user_buffer_[i+1] != caracter; i++);

		if(user_buffer_[i+1] == caracter){
			memcpy((char*)user_buffer_decode_, (char*)&user_buffer_[1], i);
			user_buffer_decode_[i] = '\n';
			return true;
		} else
			return false;

	} else {
		vPrintString("Error command!\n\r");
		for (uint16_t i = 0; i < size; ++i)
			command_char_out(user_buffer_[i]);
		return false;
	}
}

void vTaskUART(void *pvParameters) {
  {
    uart_interrupt_driver_config_t config;

    config.uart = &huart3;

    config.rx.buffer = rx_buffer_;
    config.rx.buffer_size = BUFFER_SIZE_;

    config.tx.buffer = tx_buffer_;
    config.tx.buffer_size = BUFFER_SIZE_;

    uart_interrupt_driver_init(&driver_, &config);
  }

  init_interface_uart(NULL);

  uint16_t size;
  while (true) {
    size = uart_interrupt_driver_receive(&driver_, user_buffer_, portMAX_DELAY);

    if(decode_message(size, '^')) {
    	for (uint16_t i = 0; i < (size-2); ++i)
    		command_char_in(user_buffer_decode_[i]);
    }
  }
}

/********************** end of file ******************************************/
