/*
 * taskStart.c
 *
 *  Created on: 27 jun. 2022
 *      Author: javelasco
 */

/* Project includes. */
#include "main.h"
#include "cmsis_os.h"

/* Standard includes. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/* Demo includes. */
#include "supporting_Functions.h"

/* Application includes. */
#include "app_Resources.h"
#include "appConfigs.h"
#include "taskStart.h"
#include <routine_example_command.h>


static tick_t startTime;

void vTaskStart ( void* pvParameters ) {
	vPrintString( "Begin TaskStart\n\r" );
	xSemaphoreTake( xBinarySemaphoreStartRecording, 0 );
	xSemaphoreTake( xBinarySemaphorePlayRecording, 0 );

	vPrintString( "Please keep the button pressed for 3 seconds to start recording.\n\r" );

	while( 1 ) {
		xSemaphoreTake( xBinarySemaphoreStart, 0 );

		if ( HAL_GPIO_ReadPin( USER_Btn_GPIO_Port, USER_Btn_Pin ) ) {
			vPrintString( "Button is pressed!\n\r" );

			vPrintStringAndNumber( "Number of stored messages: ", List_CountMsgs(p));
			vPrintString( "\n\r" );

			startTime = HAL_GetTick();
			while (HAL_GetTick() < startTime + RECORDING_DELAY_MS);

			if ( HAL_GPIO_ReadPin( USER_Btn_GPIO_Port, USER_Btn_Pin ) ) {
				if(List_AddMsg(&p_temp) != NULL) {
					vPrintString( "New message and node created!\n\r" );
					vTaskResume(vTaskLedHandle);
					//vPrintString( "Give semaphore StartRecording\n\r" );
					vPrintString( "Recording...\n\r" );
					osDelay(500);
					xSemaphoreTake(xMutexSerial, portMAX_DELAY);
						flagCommandSerial = false;
				    xSemaphoreGive(xMutexSerial);
					xSemaphoreGive( xBinarySemaphoreStartRecording );
					xSemaphoreTake( xBinarySemaphoreStart, portMAX_DELAY );
				} else {
					vPrintString( "The menssage and node was not created. Dont memory available!\n\r" );
					vPrintStringAndNumber( "Number of stored messages: ", List_CountMsgs(p_temp));
					vPrintString( "\n\r" );
				} //End Else
			} //End If
		} //End If

	} //End While
} //End TaskStart
