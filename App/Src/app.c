// ------ Includes -----------------------------------------------------
/* Project includes. */
#include "main.h"
#include "cmsis_os.h"

/* Standard includes. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/* Demo includes. */
#include "supporting_Functions.h"

/* Application & Tasks includes. */
#include "app.h"
#include "app_Resources.h"
#include "taskStart.h"
#include "taskUART.h"
#include "taskLed.h"
#include "taskPlayRecording.h"
#include "taskStartRecording.h"


// ------ Macros and definitions ---------------------------------------

// ------ internal data declaration ------------------------------------
/* Declare a variable of type xTaskHandle. This is used to reference tasks. */

xTaskHandle vTaskStartHandle;
xTaskHandle vTaskUARTHandle;
xTaskHandle vTaskLedHandle;
xTaskHandle vTaskStartRecordingHandle;
xTaskHandle vTaskPlayRecordingHandle;

xSemaphoreHandle xBinarySemaphoreStart;
xSemaphoreHandle xBinarySemaphoreStartRecording;
xSemaphoreHandle xBinarySemaphorePlayRecording;
//xSemaphoreHandle xBinarySemaphoreLed;

xSemaphoreHandle xMutex;
xSemaphoreHandle xMutexSerial;

xQueueHandle xQueuePlayRecording;

Msg_t* p = NULL;		/* Save message list */
Msg_t* p_temp = NULL;	/* Temporary message list */
bool flagStop=false, flagCommandSerial=true;


// ------ internal functions declaration -------------------------------

// ------ internal data definition -------------------------------------

// ------ external data definition -------------------------------------

// ------ internal functions definition --------------------------------

// ------ external functions definition --------------------------------


/*------------------------------------------------------------------*/
/* App Initialization */
void appInit( void )
{
	HAL_GPIO_WritePin(GPIOB, LD1_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, LD2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_RESET);

	p->msgIdent = 0;
	p->node = NULL;
	(p->node)->sig = NULL;
	p->totalIndexOfMsg = 0;
	p->sig = NULL;

	p_temp->msgIdent = 0;
	p_temp->node = NULL;
	(p_temp->node)->sig = NULL;
	p_temp->totalIndexOfMsg = 0;
	p_temp->sig = NULL;

	osDelay(100);

	vSemaphoreCreateBinary( xBinarySemaphoreStart );
	configASSERT( xBinarySemaphoreStart    			!=  NULL );

	vSemaphoreCreateBinary( xBinarySemaphoreStartRecording );
	configASSERT( xBinarySemaphoreStartRecording    !=  NULL );

	vSemaphoreCreateBinary( xBinarySemaphorePlayRecording );
	configASSERT( xBinarySemaphorePlayRecording 	!=  NULL );

	//vSemaphoreCreateBinary( xBinarySemaphoreLed );
	//configASSERT( xBinarySemaphoreLed 				!=  NULL );

	xMutex = xSemaphoreCreateMutex();
	configASSERT( xMutex 				!=  NULL );

	xMutexSerial = xSemaphoreCreateMutex();
	configASSERT( xMutexSerial 			!=  NULL );

	xQueuePlayRecording = xQueueCreate(QUEUE_LENGHT, sizeof(Msg_t*));
	configASSERT( xQueuePlayRecording 	!= NULL );


	BaseType_t ret;
	ret = xTaskCreate(
			vTaskStart,							/* Pointer to the function thats implement the task. */
			"Task Start",						/* Text name for the task. This is to facilitate debugging only. */
			(2 * configMINIMAL_STACK_SIZE),		/* Stack depth in words. 					*/
			NULL,								/* We are not using the task parameter.		*/
			(tskIDLE_PRIORITY + 2UL),			/* This task will run at priority 1. 		*/
			&vTaskStartHandle );				/* We are using a variable as task handle.	*/

	configASSERT( ret == pdPASS );

	ret = xTaskCreate(
			vTaskUART,							/* Pointer to the function thats implement the task. */
			"Task UART",						/* Text name for the task. This is to facilitate debugging only. */
			(2 * configMINIMAL_STACK_SIZE),		/* Stack depth in words. 					*/
			NULL,								/* We are not using the task parameter.		*/
			(tskIDLE_PRIORITY + 2UL),			/* This task will run at priority 1. 		*/
			&vTaskUARTHandle );					/* We are using a variable as task handle.	*/

	configASSERT( ret == pdPASS );

	ret = xTaskCreate(
			vTaskLed, 							/* Pointer to the function thats implement the task. */
			"Task Led", 						/* Text name for the task. This is to facilitate debugging only. */
			(2 * configMINIMAL_STACK_SIZE), 	/* Stack depth in words. 					*/
			NULL, 								/* We are not using the task parameter.		*/
			(tskIDLE_PRIORITY + 2UL), 			/* This task will run at priority 1. 		*/
			&vTaskLedHandle ); 					/* We are using a variable as task handle.	*/

	configASSERT(ret == pdPASS);

	ret = xTaskCreate(
			vTaskStartRecording, 					/* Pointer to the function thats implement the task. */
			"Task StartRecording", 					/* Text name for the task. This is to facilitate debugging only. */
			(2 * configMINIMAL_STACK_SIZE), 	/* Stack depth in words. 					*/
			NULL, 								/* We are not using the task parameter.		*/
			(tskIDLE_PRIORITY + 2UL), 			/* This task will run at priority 1. 		*/
			&vTaskStartRecordingHandle ); 			/* We are using a variable as task handle.	*/

	configASSERT(ret == pdPASS);

	ret = xTaskCreate(
			vTaskPlayRecording, 						/* Pointer to the function thats implement the task. */
			"Task PlayRecording", 					/* Text name for the task. This is to facilitate debugging only. */
			(2 * configMINIMAL_STACK_SIZE), 	/* Stack depth in words. 					*/
			NULL, 								/* We are not using the task parameter.		*/
			(tskIDLE_PRIORITY + 2UL), 			/* This task will run at priority 1. 		*/
			&vTaskPlayRecordingHandle ); 			/* We are using a variable as task handle.	*/

	configASSERT(ret == pdPASS);
}

/*------------------------------------------------------------------*-
  ---- END OF FILE -------------------------------------------------
-*------------------------------------------------------------------*/
